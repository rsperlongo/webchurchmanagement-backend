const mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        require: true
    },
});

UserSchema.methods.validatePassword = function(password) {
    return password == this.password
}

module.exports = mongoose.model('User', UserSchema);