const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const jsonwebtoken = require('jsonwebtoken');

const login = require('./routes/login');
const user = require('./routes/user');

const InitiateMongoServer = require('./config/db');

// Initiate Mongo Server
InitiateMongoServer();

const app = express();

// PORT
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

// ROUTES
app.use('/api/v1/user', user);
app.use('/api/v1/login', login);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
      error: {
        message: error.message
      }
    });
});

app.listen(PORT, (req, res) => {
    console.log(`Server started at PORT ${PORT}`);
});

module.exports = app;