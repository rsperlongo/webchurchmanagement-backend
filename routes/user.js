const express = require('express');
const mongoose = require('mongoose');
const User = require('../models/User');
const verifyToken = require('../models/VerifyToken');

const router = express.Router();

router.get('/', verifyToken, (req, res, next) => {
    User.find({ token: token})
      .exec()
      .then(docs => {
        res.status(200).json(docs);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
        next(err);
      });
  });

  router.post('/', (req, res, next) => {
      const newUser = new User({
          _id: new mongoose.Types.ObjectId(),
          name: req.body.name,
          username: req.body.username,
          password: req.body.password,
          email: req.body.email 
      });

      newUser
    .save()
    .then(result => {
        res.status(201).json({
            createdProduct: result
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
  });

  module.exports = router;

  