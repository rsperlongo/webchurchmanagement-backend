const express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

const config = require('../config/config');
const User = require('../models/User');

const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.post('/', function(req, res) {
    User.findOne({ login: req.body.login }, function(err, user) {
        if(err) return res.status(500).send('Internal server error');
        if(!user) return res.status(400).send('usuário não encontrado');

        const validatePassword = user.validatePassword(req.body.password)
        if(!validatePassword) return res.status(404).send({ auth: false, token: null});

        const token = jwt.sign({ id: user._id, email: user.email }, config.secret, {
            expiresIn: 10000
        });
        res.status(200).send({ auth: true, token: token });
    });
});

router.get('/logout', function(req, res) {
    res.status(200).send({ auth: false, token: null })
});

module.exports = router;
